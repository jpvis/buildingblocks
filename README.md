# buildingblocks

Ivanti Identity Director Service and Ivanti Automation Runbooks to export a Service including the related Runbooks.

This package allows you to get a Service with all related runbook(s) to be exported in a zip file.
This can be helpful if you need to archive your Service or to transport your Service in a DTAP environment.
Workflow:
•	Pick an Ivanti Identity Director Service from a List;
•	That Service will be exported as a building block, incl related Services (normal behavior);
•	The runbook(s) in the Service will be exported as building block(s);
•	And, if any, the Runbook(s) in the related Services will be exported as building block(s);
•	All building block(s) will be zipped (including a file relation information);
•	All building block(s) will be uploaded to GitLAB;

Requirements;
•	Ivanti Automation incl API enabled
•	Ivanti Identity Director incl API enabled
•	Review Ivanti Automation variables
•	GitLab Access